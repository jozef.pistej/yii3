<?php

use hiqdev\composer\config\Builder;
use yii\di\Container;
use yii\helpers\Yii;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

(function () {
    require_once __DIR__ . '/../vendor/autoload.php';

    $container = new Container(require Builder::path('web'));

    Yii::setContainer($container);

    $container->get('app')->run();
})();
